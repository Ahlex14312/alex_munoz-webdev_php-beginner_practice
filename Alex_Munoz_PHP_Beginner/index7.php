<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <title>Exercise Number 47</title>
</head>
<body>
<nav class="bg-dark pb-4">
        <h2 class="text-center text-white pt-4">Numbers-Counterpart Converter</h2>
    </nav>
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-header bg-success">
                        <h3 class="text-center">Number to Convert</h3>
                    </div>
                    <div class="card-body bg-dark">
                    <form method="post">
                        <label for="" class="text-success">Enter a number:</label><br>
                        <input type="text" class="form-control" name="Number"><br>
                        <button class="btn btn-danger" value="Submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <?php  

if($_POST){
    $num = $_POST['Number'];
    getLength($num);
}

function getLength($num){
    $str_length = strlen(strval($num));
    for ($ctr=0; $ctr <$str_length ; $ctr++) { 
        getNumIndex($num[$ctr]);
    }
}

function getNumIndex($digit){              
    switch($digit){
        
        case '0':
            echo "Zero ";
            break;
        case '1':
            echo "One ";
            break;
        case '2':
            echo "Two ";
            break;
        case '3':
            echo "Three ";
            break;
        case '4':
            echo "Four ";
            break;
        case '5':
            echo "Five ";
            break;
        case '6':
            echo "Six ";
            break;
        case '7':
            echo "Seven ";
            break;
        case '8':
            echo "Eight ";
            break;
        case '9':
            echo "Nine ";
            break;
                            
    }
}
?>
</body>
</html>
